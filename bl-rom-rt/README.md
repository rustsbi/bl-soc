# 博流物联网芯片ROM运行环境

[![crates.io](https://img.shields.io/crates/v/bl-rom-rt.svg)](https://crates.io/crates/bl-rom-rt)
[![Documentation](https://docs.rs/bl-rom-rt/badge.svg)](https://docs.rs/bl-rom-rt)
![License](https://img.shields.io/crates/l/bl-rom-rt.svg)

本项目是博流物联网芯片的ROM运行环境。
