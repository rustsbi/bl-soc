# Bouffalo chip Rust Hardware Abstraction Layer (HAL)

This repository contains `bl-soc` and `bl-rom-rt` packages.

## License

Project is dual licensed under MIT or Mulan-PSL v2.

```
Copyright (c) 2023 RustSBI Team
bl-soc is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details.
```
