# 博流芯片组件化外设支持库

[![crates.io](https://img.shields.io/crates/v/bl-soc.svg)](https://crates.io/crates/bl-soc)
[![Documentation](https://docs.rs/bl-soc/badge.svg)](https://docs.rs/bl-soc)
![License](https://img.shields.io/crates/l/bl-soc.svg)

本仓库是根据组件化外设思想，为博流物联网芯片提供的Rust语言支持。

## 许可协议

根据木兰宽松许可证，第2版发布。

```
Copyright (c) 2023 RustSBI Team
bl-soc is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details.
```
